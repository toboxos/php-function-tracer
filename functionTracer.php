<?php 

    // $index is at a token which is T_STRING
    // returns false if no function, else return array with all functions called in this parsing steps
    function scanFunction($tokens, $index) {
        
        $indexStart = $index;
        
        // Name of function
        $function = $tokens[$index][1] . "(";
        
        // Line of function
        $line = $tokens[$index][2];
        
        // Array of all found functions with the number of parameter of each function
        $functions = array();
        
        // Get next token after function name
        ++$index;  
        
        
        // Skip whitespaces
        for( ; is_array($tokens[$index]) and token_name($tokens[$index][0]) == "T_WHITESPACE"; ++$index );
    
        // No function because of missing '('
        if( is_array($tokens[$index]) or $tokens[$index] != '(' ) {
            return array($index, false);
        }
        
        // Go one token before function name
        --$indexStart;
        
        // Skip whitespaces backward
        for( ; is_array($tokens[$indexStart]) and token_name($tokens[$indexStart][0]) == "T_WHITESPACE"; --$indexStart );
        
        // Function definition, no function call, return
        if( is_array($tokens[$indexStart]) and token_name($tokens[$indexStart][0]) == "T_FUNCTION" ) return array($index, false);    
        
        // Scan until ')'
        while( is_array($tokens[$index]) or $tokens[$index] != ')' ) {
            ++$index;
            
            // Skip whitespaces
            for( ; is_array($tokens[$index]) and token_name($tokens[$index][0]) == "T_WHITESPACE"; ++$index );
            
            // If String token found, maybe another function call
            if( is_array($tokens[$index]) and token_name($tokens[$index][0]) == 'T_STRING' ) {
                $ret = scanFunction($tokens, $index);
                
                if( $ret[1] !== false ) {
                    
                    // Go thorugh each return function and add it to function list
                    foreach( $ret[1] as $calledFunction ) {
                        array_push($functions, $calledFunction);
                    }
                    
                    $function .= $ret[1][0][1];
                }
                
                $index = $ret[0];
            } else {
                if( is_array($tokens[$index]) ) {
                    $function .= $tokens[$index][1];
                } else {
                    $function .= $tokens[$index];
                    
                    // Whitespace between function parameters for better reading
                    if( $tokens[$index] == "," ) $function .= " ";
                }
            }
        } 
        
        array_unshift($functions, array($function, $line));
        
        return array($index, $functions);
   }

    function scanFile($file) {
       
        $functions = array();
        
        $tokens = token_get_all(file_get_contents($file));
        $index = 0;
        
         
         //var_dump($tokens);
        
    
        // Scanning until end of tokens
        while( $index < count($tokens) ) {
            
            // Skip whitespaces
            for( ; is_array($tokens[$index]) and token_name($tokens[$index][0]) == "T_WHITESPACE"; $index++ );
            
            // If String token found, maybe another function call
            if( is_array($tokens[$index]) and token_name($tokens[$index][0]) == 'T_STRING') {
                $ret = scanFunction($tokens, $index);
                
                if( $ret[1] !== false ) {
                    
                     // Go thorugh each return function and add it to function list
                    foreach( $ret[1] as $calledFunction ) {
                        array_push($functions, $calledFunction);
                    }
                }
                
                $index = $ret[0];
            }
            
            ++$index;
        }
        
        return $functions;
    }
    
    
    echo "<pre>";
    
    foreach( scanDir(".") as $file ) {
        if( $file == "." or $file == ".." ) continue;
        
        echo "<h1>" . $file . "</h1>";
        foreach( scanFile($file) as $function ) {
            echo "Line: {$function[1]}: " . htmlspecialchars($function[0]) . "\n";
        }
    }
    
    
    
    echo "</pre>";
    
    
?>